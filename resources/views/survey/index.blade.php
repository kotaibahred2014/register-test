<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ClinGroup') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/survey.css') }}">
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <style>
        .iti__flag {
            background-image: url({{asset('images/flags.png')}});
        }

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url({{asset('images/flags@2x.png')}});
            }
        }
    </style>

    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{asset('js/int-phone-num/intlTelInput.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $("div.desc").hide();
            $("div.descPerc").hide();
            $("div.descSpecificDisease").hide();
            $("input[name$='join_as_a_speaker']").click(function () {
                var test = $(this).val();
                if (test == 'Yes') {
                    $("div.desc").show();
                } else {
                    $("div.desc").hide();

                }
            });

            $("input[name$='outsource_any_of_your_activities']").click(function () {
                var test = $(this).val();
                if (test == 'Yes') {
                    $("div.descPerc").show();
                } else {
                    $("div.descPerc").hide();

                }
            });
            $("input[name$='specific_disease_radio']").click(function () {
                var test = $(this).val();
                if (test == 'Yes') {
                    $("div.descSpecificDisease").show();
                } else {
                    $("div.descSpecificDisease").hide();

                }
            });
            // $("input").intlTelInput({
            //     utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js"
            // });
            var input = document.querySelector(".phone-number");

            var iti = window.intlTelInput(input, {
                initialCountry: "LB",
                nationalMode: true,
                hiddenInput: "primary_contact_phone_number",
                utilsScript: "{{asset('js/int-phone-num/utils.js')}}",
            });

            // iti.style("display","block");
            // iti.css("display", "block");
            var handleChange = function () {
                var country_data = iti.getNumber();
                $(".phone-number").val(country_data);
            };

            input.addEventListener("countrychange", handleChange);

        });
    </script>
</head>
<body class="font-sans antialiased">
<div class="min-h-screen bg-gray-100">

    <!-- Page Heading -->
    <header class="bg-white shadow">
    </header>

    <!-- Page Content -->
    <main>
        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                        <div>
                            <x-jet-application-logo class="block h-12 w-auto"/>
                        </div>
                        <div class="container survey">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <p>
                                Celebrating the last year before its Third decade starts…<br/><br/>

                                Our 19th Anniversary!<br/><br/>


                                Born in France and operating in Africa and Asia, ClinGroup has offered health-related
                                services in research & development, quality & capacity building, and data & information
                                systems, while dedicating all of its profits to philanthropy in sustainable women
                                empowerment, children education, and health causes…<br/><br/>

                                ClinGroup wishes to celebrate its 19th anniversary in a unique way, and with each of
                                you, soldiers of the global white army, whether in regular or front-line positions. This
                                year, we are willing to share our celebration with you through offering an
                                internationally-accredited conference, free-of-charge. This conference will be tailored
                                based on your requests and topics/updates you are interested in. Please take a moment to
                                help us tailor our conference up to your expectations by sharing with us the topics that
                                you are interested in, as well as topics you would like to present to others! Thank you
                                for taking a moment to fill the below questionnaire.<br/>


                                ClinGroup Team!

                            </p>
                            <form id="survey-form" name="surveyForm" action="survey" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                <h3 class="required-label title margin-div">Company Details</h3>
                                <div class="form-row">
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <label id="company_name_label" class="required-label">Company Name</label>
                                        <input id="company_name" type="name" name="company_name" class="form-control"
                                               placeholder="Company Name" required>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3" >
                                        <label for="for_primary_contact_name" class="required-label">Primary Contact
                                            Name</label>
                                        <input id="primary_contact_name" type="name" name="primary_contact_name"
                                               class="form-control" placeholder="Primary Contact Name" required>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <label id="primary_contact_position_label" class="required-label">Primary
                                            Contact
                                            Position</label>
                                        <input id="primary_contact_position" type="name" name="primary_contact_position"
                                               class="form-control" placeholder="Primary Contact Position" required>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-row">
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <label id="email-label" class="required-label">Email address</label>
                                        <input id="primary_contact_email" type="email" name="primary_contact_email"
                                               class="form-control" aria-describedby="emailHelp"
                                               placeholder="Enter email"
                                               required>
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email
                                            with
                                            anyone else.</small>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <label id="number-label" class="required-label">Primary Contact Phone
                                            Number</label>
                                        <br/>
                                        <input id="number" type="tel" class="form-control phone-number"
                                               name="primary_contact_phone_number_input" id="number"
                                               required>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group">
                                            <label id="business-sector-label" class="required-label">Business
                                                Sector</label>
                                            <select class="selectpicker" name="nature_of_business[]"
                                                    multiple
                                                    data-live-search="true">
                                                <option value="Biopharmaceutical">Biopharmaceutical</option>
                                                <option value="Biotech/Medtech">Biotech/Medtech</option>
                                                <option value="Health Institution/Organization">Health
                                                    Institution/Organization
                                                </option>
                                                <option value="Health Faculty">Health Faculty</option>
                                                <option value="Medical Society/Syndicate/Investigator">Medical
                                                    Society/Syndicate/Investigator
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
{{--                                <div class="margin-div">--}}
{{--                                    <h4 class="required-label subtitle">Nature(s) of business</h4>--}}
{{--                                    <ul class="checkboxlabel">--}}
{{--                                        <li class="checkbox" style="list-style: none;"><label> <input--}}
{{--                                                    name="nature_of_business[]" value="Pharmaceutical Company"--}}
{{--                                                    type="checkbox">Pharmaceutical Company</label></li>--}}
{{--                                        <li class="checkbox" style="list-style: none;"><label> <input--}}
{{--                                                    name="nature_of_business[]" value="Biotech"--}}
{{--                                                    type="checkbox">Biotech</label></li>--}}
{{--                                        <li class="checkbox" style="list-style: none;"><label> <input--}}
{{--                                                    name="nature_of_business[]" value="Hospital"--}}
{{--                                                    type="checkbox">Hospital</label></li>--}}
{{--                                        <li class="checkbox" style="list-style: none;"><label> <input--}}
{{--                                                    name="nature_of_business[]" value="University" type="checkbox">University</label>--}}
{{--                                        </li>--}}
{{--                                        <li class="checkbox" style="list-style: none;"><label> <input--}}
{{--                                                    name="nature_of_business[]" value="Principal Investigator"--}}
{{--                                                    type="checkbox">Principal Investigator</label></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}

                                <h3 class="title margin-div">I- The Conference</h3>

                                <div>
                                    <H5 class="question">1- What’s your prefered conference channel? (you may choose
                                        more
                                        than 1 answer)</H5>
                                    <ul class="checkboxlabel">
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="preferred_conference_channel[]" value="Live" type="checkbox"
                                                    class="userRatings">Live </label></li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="preferred_conference_channel[]" value="Online" type="checkbox"
                                                    class="userRatings">Online</label></li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="preferred_conference_channel[]"
                                                    value="Hybrid model conference (Live and online)" type="checkbox"
                                                    class="userRatings">Hybrid model conference (Live and online)
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                                <br/>

                                <div>
                                    <H5 class="question">2- What are the topics you are interested in attending?</H5>
                                    <ul class="checkboxlabel">
                                        <li class="checkbox" style="list-style: none;">
                                            <label id="business-sector-label" class="">Clinical/Medical Research</label>
                                            <select class="selectpicker" name="topics_interested[]"
                                                    multiple
                                                    data-live-search="true">
                                                <option value="Real">Real</option>
                                                <option value="Virtual">Virtual</option>
                                                <option value="Decentralized">Decentralized</option>
                                            </select>
                                        </li>
                                        <li class="checkbox" style="list-style: none;">
                                            <label id="business-sector-label" class="">Patient Advocacy/Centricity</label>
                                            <select class="selectpicker" name="topics_interested[]"
                                                    multiple
                                                    data-live-search="true">
                                                <option value="Acromegaly">Acromegaly</option>
                                                <option value="Acute Myeloid Leukemia">Acute Myeloid Leukemia</option>
                                                <option value="Alpha-1 Antitrypsin Deficiency">Alpha-1 Antitrypsin Deficiency</option>
                                                <option value="Alzheimer’s Disease">Alzheimer’s Disease</option>
                                                <option value="Amyloidosis">Amyloidosis</option>
                                                <option value="Amyotrophic Lateral Sclerosis (ALS)">Amyotrophic Lateral Sclerosis (ALS)</option>
                                                <option value="Ankylosing Spondylitis">Ankylosing Spondylitis</option>
                                                <option value="Aplastic Anemia">Aplastic Anemia</option>
                                                <option value="Asthma">Asthma</option>
                                                <option value="Batten Disease">Batten Disease</option>
                                                <option value="Bile Duct Cancer">Bile Duct Cancer</option>
                                                <option value="Bladder Cancer">Bladder Cancer</option>
                                                <option value="Brain Cancer">Brain Cancer</option>
                                                <option value="Breast Cancer">Breast Cancer</option>
                                                <option value="Cancer Genetic and Genomic Testing">Cancer Genetic and Genomic Testing</option>
                                                <option value="Cardiac Arrhythmias">Cardiac Arrhythmias</option>
                                                <option value="Cervical Cancer">Cervical Cancer</option>
                                                <option value="Chronic Obstructive Pulmonary Disease (COPD)">Chronic Obstructive Pulmonary Disease (COPD)</option>
                                                <option value="Chronic Pain">Chronic Pain</option>
                                                <option value="Coronary Artery Disease (CAD)">Coronary Artery Disease (CAD)</option>
                                                <option value="COVID-19">COVID-19</option>
                                                <option value="Crohn’s Disease">Crohn’s Disease</option>
                                                <option value="Cushing’s Syndrome">Cushing’s Syndrome</option>
                                                <option value="Cystic Fibrosis">Cystic Fibrosis</option>
                                                <option value="Cystinosis">Cystinosis</option>
                                                <option value="Diabetes Type 1">Diabetes Type 1</option>
                                                <option value="Diabetes Type 2">Diabetes Type 2</option>
                                                <option value="Electrolyte Imbalance">Electrolyte Imbalance</option>
                                                <option value="Epilepsy">Epilepsy</option>
                                                <option value="Esophageal Cancer">Esophageal Cancer</option>
                                                <option value="Fabry Disease">Fabry Disease</option>
                                                <option value="Gaucher Disease">Gaucher Disease</option>
                                                <option value="Graft vs Host Disease">Graft vs Host Disease</option>
                                                <option value="Growth Hormone Deficiency">Growth Hormone Deficiency</option>
                                                <option value="Head & Neck Cancer">Head & Neck Cancer</option>
                                                <option value="Heart Failure">Heart Failure</option>
                                                <option value="Hemophilia">Hemophilia</option>
                                                <option value="Hepatitis B">Hepatitis B</option>
                                                <option value="Hepatitis C">Hepatitis C</option>
                                                <option value="Hepatocellular Carcinoma">Hepatocellular Carcinoma</option>
                                                <option value="Hereditary Angioedema">Hereditary Angioedema</option>
                                                <option value="HIV, AIDS and Prevention">HIV, AIDS and Prevention</option>
                                                <option value="Homozygous Familial Hypercholesterolemia">Homozygous Familial Hypercholesterolemia</option>
                                                <option value="Huntington’s Disease">Huntington’s Disease</option>
                                                <option value="Hyperlipidemia">Hyperlipidemia</option>
                                                <option value="Hyperparathyroidism">Hyperparathyroidism</option>
                                                <option value="Idiopathic Thrombocytopenic Purpura">Idiopathic Thrombocytopenic Purpura</option>
                                                <option value="Inherited or Acquired Lipodystrophy">Inherited or Acquired Lipodystrophy</option>
                                                <option value="Juvenile Idiopathic Systemic Arthritis">Juvenile Idiopathic Systemic Arthritis</option>
                                                <option value="Lupus">Lupus</option>
                                                <option value="Macular Degeneration">Macular Degeneration</option>
                                                <option value="Mast Cell Activation Disorders">Mast Cell Activation Disorders</option>
                                                <option value="Melanoma">Melanoma</option>
                                                <option value="Metastatic Bladder Cancer">Metastatic Bladder Cancer</option>
                                                <option value="Metastatic Breast Cancer">Metastatic Breast Cancer</option>
                                                <option value="Metastatic Colorectal Cancer">Metastatic Colorectal Cancer</option>
                                                <option value="Metastatic Gastric Cancer">Metastatic Gastric Cancer</option>
                                                <option value="Metastatic Melanoma">Metastatic Melanoma</option>
                                                <option value="Metastatic Prostate Cancer">Metastatic Prostate Cancer</option>
                                                <option value="Migraine">Migraine</option>
                                                <option value="Multiple Myeloma">Multiple Myeloma</option>
                                                <option value="Multiple Sclerosis">Multiple Sclerosis</option>
                                                <option value="Muscular Dystrophy">Muscular Dystrophy</option>
                                                <option value="Myasthenia Gravis">Myasthenia Gravis</option>
                                                <option value="Myelodysplastic Syndromes">Myelodysplastic Syndromes</option>
                                                <option value="Myeloproliferative Disorders">Myeloproliferative Disorders</option>
                                                <option value="Narcolepsy">Narcolepsy</option>
                                                <option value="Neoplasm Related Pain">Neoplasm Related Pain</option>
                                                <option value="Niemann-Pick Disease">Niemann-Pick Disease</option>
                                                <option value="Non-Small Cell Lung Cancers">Non-Small Cell Lung Cancers</option>
                                                <option value="Osteoporosis">Osteoporosis</option>
                                                <option value="Ovarian Cancer">Ovarian Cancer</option>
                                                <option value="Pancreatic Cancer">Pancreatic Cancer</option>
                                                <option value="Paraganglioma & Pheochromocytoma">Paraganglioma & Pheochromocytoma</option>
                                                <option value="Parkinson’s Disease">Parkinson’s Disease</option>
                                                <option value="Periodic Paralysis">Periodic Paralysis</option>
                                                <option value="Peripheral Vascular Disease">Peripheral Vascular Disease</option>
                                                <option value="Pompe Disease">Pompe Disease</option>
                                                <option value="Primary Immunodeficiency">Primary Immunodeficiency</option>
                                                <option value="Prostate Cancer">Prostate Cancer</option>
                                                <option value="Psoriatic Arthritis">Psoriatic Arthritis</option>
                                                <option value="Pulmonary Fibrosis">Pulmonary Fibrosis</option>
                                                <option value="Pulmonary Hypertension">Pulmonary Hypertension</option>
                                                <option value="Renal Cell Carcinoma">Renal Cell Carcinoma</option>
                                                <option value="Rheumatoid Arthritis">Rheumatoid Arthritis</option>
                                                <option value="Sarcoma of the Bone">Sarcoma of the Bone</option>
                                                <option value="Sickle Cell Disease">Sickle Cell Disease</option>
                                                <option value="Skin Cancer">Skin Cancer</option>
                                                <option value="Small Cell Lung Cancer">Small Cell Lung Cancer</option>
                                                <option value="Soft Tissue Sarcoma">Soft Tissue Sarcoma</option>
                                                <option value="Spinal Muscular Atrophy">Spinal Muscular Atrophy</option>
                                                <option value="Stroke">Stroke</option>
                                                <option value="Testicular Cancer">Testicular Cancer</option>
                                                <option value="Thalassemia">Thalassemia</option>
                                                <option value="Thrombocytopenia">Thrombocytopenia</option>
                                                <option value="Thyroid Cancer">Thyroid Cancer</option>
                                                <option value="Ulcerative Colitis">Ulcerative Colitis</option>
                                                <option value="Uterine Cancers">Uterine Cancers</option>
                                                <option value="Uveitis">Uveitis</option>
                                                <option value="Virology Testing Fund">Virology Testing Fund</option>
                                                <option value="Wilson Disease">Wilson Disease</option>
                                                <option value="Acromegaly">Acromegaly</option>

                                            </select>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="topics_interested[]" value="Market Access" type="checkbox"
                                                    class="userRatings"> Market Access </label></li>

                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]" value="Pharmacovigilence" type="checkbox"
                                                    class="userRatings"> Pharmacovigilence </label></li>

                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]" value="Nutrivigilence" type="checkbox"
                                                    class="userRatings"> Nutrivigilence </label></li>

                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]" value="Regulatory Affairs"
                                                    type="checkbox"
                                                    class="userRatings"> Regulatory Affairs </label></li>
                                        <br/>

                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]"
                                                    value="E-health" type="checkbox"
                                                    class="userRatings"> E-health
                                            </label>
                                        </li>
                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]"
                                                    value="Artificial Intelligence"
                                                    type="checkbox" class="userRatings"> Artificial Intelligence </label></li>
                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]"
                                                    value="Information Management"
                                                    type="checkbox" class="userRatings"> Information Management </label></li>
                                        <li class="checkbox" ><label> <input
                                                    name="topics_interested[]"
                                                    value="Real World Evidence"
                                                    type="checkbox" class="userRatings"> Real World Evidence </label></li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="topics_interested[]"
                                                    value="Patient Research Outcome"
                                                    type="checkbox" class="userRatings"> Patient Research Outcome </label></li>
{{--                                        <br/>--}}
                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]"
                                                    value="Alliance"
                                                    type="checkbox" class="userRatings"> Alliance </label></li>
                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]"
                                                    value="Partnering"
                                                    type="checkbox" class="userRatings"> Partnering </label></li>
                                        <li class="checkbox" style="display: inline"><label> <input
                                                    name="topics_interested[]"
                                                    value="Contracting"
                                                    type="checkbox" class="userRatings"> Contracting  </label></li>
                                        <li class="checkbox" ><label> <input
                                                    name="topics_interested[]"
                                                    value="Quality"
                                                    type="checkbox" class="userRatings"> Quality </label></li>
                                        <li class="checkbox" style="list-style: none;">
                                            <label id="business-sector-label" class="">Emerging Region :</label>
                                        </li>
                                        <select class="selectpicker" name="emerging_region"
                                                data-live-search="true">
                                            <option value="" disabled selected>Select Country</option>
                                            <option value="Afganistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bonaire">Bonaire</option>
                                            <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Canary Islands">Canary Islands</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Channel Islands">Channel Islands</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Cocos Island">Cocos Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote DIvoire">Cote DIvoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curaco">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="French Guiana">French Guiana</option>
                                            <option value="French Polynesia">French Polynesia</option>
                                            <option value="French Southern Ter">French Southern Ter</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Great Britain">Great Britain</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Hawaii">Hawaii</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="India">India</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle of Man">Isle of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="Korea North">Korea North</option>
                                            <option value="Korea Sout">Korea South</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mayotte">Mayotte</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Midway Islands">Midway Islands</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Montserrat">Montserrat</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Nambia">Nambia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherland Antilles">Netherland Antilles</option>
                                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                            <option value="Nevis">Nevis</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau Island">Palau Island</option>
                                            <option value="Palestine">Palestine</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Phillipines">Philippines</option>
                                            <option value="Pitcairn Island">Pitcairn Island</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                                            <option value="Republic of Serbia">Republic of Serbia</option>
                                            <option value="Reunion">Reunion</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="St Barthelemy">St Barthelemy</option>
                                            <option value="St Eustatius">St Eustatius</option>
                                            <option value="St Helena">St Helena</option>
                                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St Maarten">St Maarten</option>
                                            <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                                            <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                                            <option value="Saipan">Saipan</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="Samoa American">Samoa American</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Tahiti">Tahiti</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Erimates">United Arab Emirates</option>
                                            <option value="United States of America">United States of America</option>
                                            <option value="Uraguay">Uruguay</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City State">Vatican City State</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                            <option value="Wake Island">Wake Island</option>
                                            <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zaire">Zaire</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>

                                        <div class="radiobuttons margin-div">
                                            <h5 class="">Specific Disease Profile in Research in the Emerging Region  </h5>
                                            <div style="margin-left: 15px;">
                                                  Yes<input type="radio" name="specific_disease_radio" value="Yes"/>
                                                No<input type="radio"  name="specific_disease_radio" value="No"/>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col descSpecificDisease">
                                            <label id="specific_disease_profile_label" class="">If Yes, please specify:</label>
                                            <textarea name="specific_disease_profile" type="text" class="form-control" cols="30" rows="5"
                                                   placeholder=""></textarea>
                                        </div>
{{--                                        <li>--}}
{{--                                            <label class="question">Please specify Therapeutic area :--}}
{{--                                                <input list="therapeutic_areas_list" name="therapeutic_areas"--}}
{{--                                                       class="form-control"/>--}}
{{--                                            </label>--}}
{{--                                            <datalist id="therapeutic_areas_list">--}}
{{--                                                <option value="Cardiovascular diseases">--}}
{{--                                                <option value="Autoimmune diseases">--}}
{{--                                                <option value="Pediatric and Rare diseases">--}}
{{--                                                <option value="Gyneco-Oncology ">--}}
{{--                                                <option value="Endocrinology ">--}}
{{--                                                <option value="Hematology ">--}}
{{--                                                <option value="Other: ">--}}
{{--                                            </datalist>--}}
{{--                                        </li>--}}
                                    </ul>
                                </div>
                                <br/>

                                <div class="">
                                    <h5 id="label_topics_you_suggest" class="question">What other topics do you
                                        suggest?
                                        Please list them :</h5>
                                    <ul>
                                        <li>
                                            <input type="text" name="topics_you_suggest_1"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                        <li>
                                            <input type="text" name="topics_you_suggest_2"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                        <li>
                                            <input type="text" name="topics_you_suggest_3"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                    </ul>
                                </div>
                                <br/>
                                <div class="radiobuttons">
                                    <h5 class="question required-label">Would you (or a colleague) like to join as a
                                        speaker? </h5>
                                    <div>
                                        Yes<input type="radio" name="join_as_a_speaker" value="Yes"/>
                                        No<input type="radio" name="join_as_a_speaker" value="No"/>
                                    </div>
                                </div>

                                <div class="message margin-div desc">
                                    <label id="label_topics_you_suggest_to_deliver" class="question">Please list
                                        hereafter
                                        the topic(s) you suggest to deliver</label>
                                    <textarea class="form-control" name="topics_you_suggest_to_deliver"
                                              id="topics_you_suggest_to_deliver" cols="30" rows="5"
                                              placeholder="List them here"></textarea>
                                </div>

                                <div class="radiobuttons margin-div desc">
                                    <h5 class="question">Are you available between the 18th and 21st of April 2021?</h5>
                                    <div>
                                        Yes<input type="radio" name="available_between_the_18th_and_21st" value="Yes"/>
                                        No<input type="radio" name="available_between_the_18th_and_21st" value="No"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="col desc">
                                    <label id="suggest_a_colleague_label" class="question">Please suggest a colleague
                                        who
                                        might be interested in joining the conference as a speaker</label>
                                    <input name="suggest_a_colleague" type="text" class="form-control"
                                           placeholder="suggest a colleague">
                                </div>
                                <br/>
                                <p>
                                    *If your answer is Yes, please provide the syllabus and modules of the topic within
                                    2 weeks in order to prepare for the CPD accreditation that we are willing to offer
                                    as a gift to all attendees.
                                    For more information, please do not hesitate to reach out to ckhawand@clingroup.net
                                </p>
                                <br/>
                                <h3 style="margin-top: 30px" class="title">II- Help Us Helping you</h3>
                                <p>
                                    As part of our commitment to continuous development and to customer focus, we would
                                    always love to hear how we can assist you in achieving your business goals.
                                    Please take 5 additional minutes to answer the following questions:

                                </p>
                                <h4 class="margin-div title">Let us know a little more about you:</h4>
                                <br/>
                                <div class="radiobuttons">
                                    <h5 class="question">Do you outsource any of your activities to external part-time
                                        staff? </h5>
                                    <div>
                                        Yes<input type="radio" name="outsource_any_of_your_activities" value="Yes"/>
                                        No<input type="radio" name="outsource_any_of_your_activities" value="No"/>
                                    </div>
                                </div>

                                <div class="col margin-div descPerc">
                                    <label id="percentage_label" class="question ">If yes, please indicate what
                                        percentage:</label>
                                    <input name="percentage" type="number" class="form-control"
                                           placeholder="percentage">
                                </div>
                                <div class="radiobuttons margin-div">
                                    <h5 class="question">Do you have an Internal Training Plan? </h5>
                                    <div>
                                        Yes<input type="radio" name="internal_training_plan" value="Yes"/>
                                        No<input type="radio" name="internal_training_plan" value="No"/>
                                        {{--                                        N/A<input type="radio" name="internal_training_plan" value="N/A"/>--}}
                                    </div>

                                </div>
                                <br/>
                                <h3 class="title">What are you looking for in 2021?</h3>
                                <div>
                                    <H5 class="question">1. What services would you consider for your upcoming
                                        projects?</H5>
                                    <ul class="checkboxlabel">
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]"
                                                    value="Clinical Research Management"
                                                    type="checkbox" class="userRatings"> Clinical Research
                                                Management</label></li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]"
                                                    value="Clinical Logistics Support"
                                                    type="checkbox" class="userRatings"> Clinical Logistics Support
                                            </label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]"
                                                    value="Market Access, PV and Regulatory Affairs" type="checkbox"
                                                    class="userRatings"> Market Access, PV and Regulatory Affairs
                                            </label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]"
                                                    value="Telemedicine and Mobile Nursing Services" type="checkbox"
                                                    class="userRatings"> Telemedicine and Mobile Nursing Services
                                            </label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]"
                                                    value="Data Management and Medical Writing" type="checkbox"
                                                    class="userRatings"> Data Management and Medical Writing </label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]"
                                                    value="Information Systems and Digitalisation" type="checkbox"
                                                    class="userRatings"> Information Systems and Digitalisation </label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]" value="Quality and Consultancy"
                                                    type="checkbox" class="userRatings"> Quality and Consultancy
                                            </label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]" value="Capacity Building"
                                                    type="checkbox" class="userRatings"> Capacity Building </label></li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_services[]" value="Staffing" type="checkbox"
                                                    class="userRatings"> Staffing </label></li>
                                    </ul>
                                </div>
                                <div>
                                    <H5 class="question">2. In which region are you considering to run your upcoming
                                        project(s)? </H5>
                                    <ul class="checkboxlabel">
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_regions[]" value="GCC Countries"
                                                    type="checkbox"
                                                    class="userRatings">GCC Countries</label></li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_regions[]" value="Middle East Countries"
                                                    type="checkbox" class="userRatings">Middle East Countries</label>
                                        </li>
                                        <li class="checkbox" style="list-style: none;"><label> <input
                                                    name="upcoming_project_regions[]" value="African Countries"
                                                    type="checkbox" class="userRatings">African Countries</label></li>
                                    </ul>
                                </div>

                                <div id="custom-selector">
                                    <div class="form-group">
                                        <h5 class="question">3. In which Therapeutic
                                            Area(s)</h5>
                                        <select class="selectpicker" name="therapeutic_areas_upcoming_projects[]"
                                                multiple
                                                data-live-search="true">
                                            <option value="Cardiovascular/Metabolic">Cardiovascular/Metabolic</option>
                                            <option value="Neurology">Neurology</option>
                                            <option value=" Endocrinology and Metabolic Diseases"> Endocrinology and
                                                Metabolic Diseases
                                            </option>
                                            <option value="Dermatology">Dermatology</option>
                                            <option value="Infectious Diseases">Infectious Diseases</option>
                                            <option value="Gastroenterology">Gastroenterology</option>
                                            <option value="Oncology">Oncology</option>
                                            <option value="Hematology ">Hematology</option>
                                            <option value="Rare Diseases ">Rare Diseases</option>
                                            <option value="Gynecology ">Gynecology</option>
                                            <option value="Respiratory ">Respiratory</option>
                                            <option value="Nephrology, Urinary and Renal ">Nephrology, Urinary and Renal
                                            </option>
                                            <option value="Rheumatology">Rheumatology</option>
                                            <option value="Ophthalmology">Ophthalmology</option>
                                        </select>
                                    </div>
                                </div>
                                <br/>
                                <div>
                                    <H5 class="question">4. What’s the estimated timeline of your upcoming project?</H5>
                                    <input list="upcoming_project_estimated_time" name="upcoming_project_estimated_time"
                                           class="form-control col-sm-6"/>
                                    <datalist id="upcoming_project_estimated_time">
                                        <option value="3 months">
                                        <option value="6 months">
                                        <option value="1 year">
                                        <option value="Other: ">
                                    </datalist>
                                </div>
                                <br/>
                                <H5 class="question">5. When choosing a vendor, what’s your order of priorities? (rank
                                    from
                                    1 to 6)</H5>

                                <div class="form-group row">
                                    <label for="example-number-input"
                                           class="col-sm-6 col-form-label ">Quality</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="number" min="1" max="6"
                                               name="quality_rank">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input"
                                           class="col-sm-6 col-form-label ">Exposure</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="number" min="1" max="6"
                                               name="exposure_rank">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-6 col-form-label ">Resources
                                        qualification and background</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="number" min="1" max="6"
                                               name="resources_qualification_and_background_rank">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-6 col-form-label ">Cost</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="number" min="1" max="6"
                                               name="cost_rank">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-6 col-form-label ">Expertise and
                                        years of experience in the field</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="number" min="1" max="6"
                                               name="expertise_and_years_of_experience_in_the_field_rank">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-6 col-form-label ">Time to
                                        deliver and respect of deadlines and milestones</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="number" min="1" max="6"
                                               name="time_to_deliver_and_respect_of_deadlines_and_milestones_rank">
                                    </div>
                                </div>

                                <br/>

                                <h3 class="title">
                                    We would like to hear from you!
                                </h3>

                                <div class=" margin-div">
                                    <H5 id="label_topics_you_suggest" class="question col-lg-10">6. What other
                                        services are you
                                        looking for?
                                        Please tell us your suggestions and what services ClinGroup can develop in order
                                        to
                                        fulfill your needs in 2021 :</H5>
                                    <ul>
                                        <li>
                                            <input type="text" name="other_services_1"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                        <li>
                                            <input type="text" name="other_services_2"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                        <li>
                                            <input type="text" name="other_services_3"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                    </ul>
                                </div>
                                <div class=" margin-div">
                                    <H5 id="services_and_digital_innovation_label" class="question col-lg-10">7. What
                                        e-services
                                        and digital innovation needs do you foresee?</H5>
                                    <ul>
                                        <li>
                                            <input type="text" name="e-services_and_digital_innovation_1"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                        <li>
                                            <input type="text" name="e-services_and_digital_innovation_2"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                        <li>
                                            <input type="text" name="e-services_and_digital_innovation_3"
                                                   class="form-control col-md-6 margin-div"
                                            >
                                        </li>
                                    </ul>
                                </div>
                                <p class="margin-div">
                                    Book a call with our BD representatives to learn more about our services, offerings
                                    and how we can help!
                                    Link to book <a href="#">bd@clingroup.net</a> calendar for an introductory call
                                </p>
                                <p class="margin-div">
                                    Thank you for your time! We look forward to seeing you at
                                    ClinGroup’s Annual conference as an attendee and/or speaker!</p>
                                <h5 class="margin-div title">
                                    ClinGroup EuroMed (France, Africa and Middle East)</h5>

                                <div class="margin-div">
                                    <button id="submit" type="submit" class="btn btn-secondary" style="float:right">
                                        Submit
                                    </button>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>
