<x-app-layout>
    <div class="container">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card table-responsive">
                        <div class="card-body">
                            {!! $dataTable->table(['class' => 'table table-striped table-bordered'], ['style' =>' table-layout: fixed;width: "100%";']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('js')
        {!! $dataTable->scripts() !!}
    @endsection
</x-app-layout>
