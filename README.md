
## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__ 
- Run __npm install__ 
- Run __npm run dev__ 
- That's it: launch the main URL. 

notes:
if you are using xampp :
- Run __php artisan serve__ 

if using laragon no need to do that



---
