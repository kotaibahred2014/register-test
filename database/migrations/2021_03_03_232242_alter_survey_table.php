<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
//            $table->dropColumn('syria_price');
            $table->string('emerging_region')->after('topics_interested')->nullable();
            $table->text('specific_disease_profile')->after('emerging_region')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function (Blueprint $table) {
//            $table->decimal('syria_price');
            $table->dropColumn('emerging_region');
            $table->dropColumn('specific_disease_profile');
        });
    }
}
