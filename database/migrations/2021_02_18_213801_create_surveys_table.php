<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('primary_contact_name');
            $table->string('primary_contact_position');
            $table->string('primary_contact_email');
            $table->string('primary_contact_phone_number');
            $table->longText('nature_of_business');
            $table->longText('preferred_conference_channel')->nullable();
            $table->longText('topics_interested')->nullable();
            $table->longText('therapeutic_areas')->nullable();
            $table->longText('topics_you_suggest_1')->nullable();
            $table->longText('topics_you_suggest_2')->nullable();
            $table->longText('topics_you_suggest_3')->nullable();
            $table->string('join_as_a_speaker')->nullable();
            $table->longText('topics_you_suggest_to_deliver')->nullable();
            $table->string('available_between_the_18th_and_21st')->nullable();
            $table->longText('suggest_a_colleague')->nullable();
            $table->string('outsource_any_of_your_activities')->nullable();
            $table->string('percentage')->nullable();
            $table->string('internal_training_plan')->nullable();
            $table->longText('upcoming_project_services')->nullable();
            $table->longText('upcoming_project_regions')->nullable();
            $table->longText('therapeutic_areas_upcoming_projects')->nullable();
            $table->longText('upcoming_project_estimated_time')->nullable();
            $table->longText('quality_rank')->nullable();
            $table->longText('exposure_rank')->nullable();
            $table->longText('resources_qualification_and_background_rank')->nullable();
            $table->longText('cost_rank')->nullable();
            $table->longText('expertise_and_years_of_experience_in_the_field_rank')->nullable();
            $table->longText('time_to_deliver_and_respect_of_deadlines_and_milestones_rank')->nullable();
            $table->longText('other_services_1')->nullable();
            $table->longText('other_services_2')->nullable();
            $table->longText('other_services_3')->nullable();
            $table->longText('e-services_and_digital_innovation_1')->nullable();
            $table->longText('e-services_and_digital_innovation_2')->nullable();
            $table->longText('e-services_and_digital_innovation_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
