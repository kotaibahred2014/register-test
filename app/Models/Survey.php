<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;
    protected $guarded = ['_token'];
    protected $casts = [
        'nature_of_business' => 'array',
        'preferred_conference_channel' => 'array',
        'topics_interested' => 'array',
        'upcoming_project_services' => 'array',
        'upcoming_project_regions' => 'array',
        'therapeutic_areas_upcoming_projects' => 'array',
    ];
}
