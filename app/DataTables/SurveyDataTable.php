<?php

namespace App\DataTables;

use App\Models\Survey;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SurveyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Survey $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Survey $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('survey-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy(1)
                    ->responsive()
                    ->buttons(
                        Button::make('excel'),
                        Button::make('csv'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
        'id',
            'company_name',
            'primary_contact_name',
            'primary_contact_position',
            'primary_contact_email',
            'primary_contact_phone_number',
            'nature_of_business',
            'preferred_conference_channel',
            'topics_interested',
            'emerging_region',
            'specific_disease_profile',
            'therapeutic_areas',
            'topics_you_suggest_1',
            'topics_you_suggest_2',
            'topics_you_suggest_3',
            'join_as_a_speaker',
            'topics_you_suggest_to_deliver',
            'available_between_the_18th_and_21st',
            'suggest_a_colleague',
            'outsource_any_of_your_activities',
            'percentage',
            'internal_training_plan',
            'upcoming_project_services',
            'upcoming_project_regions',
            'therapeutic_areas_upcoming_projects',
            'upcoming_project_estimated_time',
            'quality_rank',
            'exposure_rank',
            'resources_qualification_and_background_rank',
            'cost_rank',
            'expertise_and_years_of_experience_in_the_field_rank',
            'time_to_deliver_and_respect_of_deadlines_and_milestones_rank',
            'other_services_1',
            'other_services_2',
            'other_services_3',
            'e-services_and_digital_innovation_1',
            'e-services_and_digital_innovation_2',
            'e-services_and_digital_innovation_3',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Survey_' . date('YmdHis');
    }
}
