<?php

namespace App\Http\Controllers;

use App\DataTables\SurveyDataTable;
use App\Models\Survey;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    //
    public function index(SurveyDataTable $dataTable){

        return $dataTable->render('layouts.index');

    }
}
