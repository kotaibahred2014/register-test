<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('survey.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Survey $survey)
    {
//        dd(\request());
        $data = request()->validate([
            'company_name' => 'required',
            'primary_contact_name' => 'required',
            'primary_contact_position' => 'required',
            'primary_contact_email' => 'required',
            'primary_contact_phone_number' => 'required',
            'nature_of_business' => 'required',
            'preferred_conference_channel' => ['sometimes', 'nullable'],
            'topics_interested' => ['sometimes', 'nullable'],
            'emerging_region' => ['sometimes', 'nullable'],
            'specific_disease_profile' => ['sometimes', 'nullable'],
            'therapeutic_areas' => ['sometimes', 'nullable'],
            'topics_you_suggest_1' => ['sometimes', 'nullable'],
            'topics_you_suggest_2' => ['sometimes', 'nullable'],
            'topics_you_suggest_3' => ['sometimes', 'nullable'],
            'join_as_a_speaker' => 'required',
            'topics_you_suggest_to_deliver' => ['sometimes', 'nullable'],
            'available_between_the_18th_and_21st' => ['sometimes', 'nullable'],
            'suggest_a_colleague' => ['sometimes', 'nullable'],
            'outsource_any_of_your_activities' => ['sometimes', 'nullable'],
            'percentage' => ['sometimes', 'nullable'],
            'internal_training_plan' => ['sometimes', 'nullable'],
            'upcoming_project_services' => ['sometimes', 'nullable'],
            'upcoming_project_regions' => ['sometimes', 'nullable'],
            'therapeutic_areas_upcoming_projects' => ['sometimes', 'nullable'],
            'upcoming_project_estimated_time' => ['sometimes', 'nullable'],
            'quality_rank' => ['sometimes', 'nullable'],
            'exposure_rank' => ['sometimes', 'nullable'],
            'resources_qualification_and_background_rank' => ['sometimes', 'nullable'],
            'cost_rank' => ['sometimes', 'nullable'],
            'expertise_and_years_of_experience_in_the_field_rank' => ['sometimes', 'nullable'],
            'time_to_deliver_and_respect_of_deadlines_and_milestones_rank' => ['sometimes', 'nullable'],
            'other_services_1' => ['sometimes', 'nullable'],
            'other_services_2' => ['sometimes', 'nullable'],
            'other_services_3' => ['sometimes', 'nullable'],
            'e-services_and_digital_innovation_1' => ['sometimes', 'nullable'],
            'e-services_and_digital_innovation_2' => ['sometimes', 'nullable'],
            'e-services_and_digital_innovation_3' => ['sometimes', 'nullable'],
        ]);
        $response = $survey->create($data);
        return view('survey.thanks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
